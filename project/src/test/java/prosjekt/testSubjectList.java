package prosjekt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import project.Subject;
import project.SubjectList;

class testSubjectList {
	private Subject java;
	
	public void setup() {
		java = new Subject();
		java.setName("java");
	}

	@Test
	public void testAddSubject() {
		List<Integer> grades = new ArrayList<Integer>();
		List<Integer> expectedGrades = new ArrayList<Integer>();

		SubjectList subList = new SubjectList();
		subList.addSubject("matte2", 2);
		subList.addSubject("matte3", 4);
		subList.addSubject("matte4", 1);
		
		//Test at karakteren 'E' legges til i matte4-objektet
		assertTrue(SubjectList.getAllSubjects().get("matte4").getGrades().contains(1));
		
		//Test at 
		subList.addSubject("matte4", 3);
		grades = subList.getAllSubjects().get("matte4").getGrades();
		expectedGrades.add(1);
		expectedGrades.add(3);
		assertEquals(expectedGrades, grades, "alle karakterer ble ikke lagt til riktig");

		assertThrows(IllegalArgumentException.class, () -> {
			subList.addSubject("java", 8);
		});
		
	}
	

}
