package prosjekt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import project.GradeFiles;
import project.Student;
import project.Subject;

class GradeFileTest {
	private Student andrea;
	private Subject testfag;
	
	
	@BeforeEach
	public void setup() {
		andrea = new Student();
		testfag = new Subject();
		testfag.setName("testfag");
	}
	
	
	private Subject getSub() {
		Student andrea = new Student();
		andrea.setName("Andrea Huus");
		andrea.addSubject("java", 'B');
		testfag.addGrade(4);
		System.out.println(testfag);
		System.out.println(testfag.getGrades());
		return testfag;
	}
	private char getStringRep() {
		return 'B';
	}

	
	@Test
	public void testWriteToOS() {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Subject sub = getSub();
		GradeFiles gf = new GradeFiles();
		char expected = getStringRep();
		gf.writeGradelist('B', os);
		String actual = new String(os.toByteArray());
		char actual2 = actual.charAt(0);
		
		//test at grade skrives riktig til fil
		assertEquals(expected, actual2, "Ble ikke skrevet riktig til fil");

		
		//test at det ikke kastes feilmelding n�r det skrives til fil
		Assertions.assertDoesNotThrow(() -> gf.writeGradelist(sub.getName(), 'C'), "Feilmelding n�r man skriver til fil");
		
	}
	
	@Test
	public void testReadFromIS() throws UnsupportedEncodingException{
		String str = "";
		str += getStringRep();
		InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));

		GradeFiles gf = new GradeFiles();
		Subject actual = gf.readGradelist("testfag", is);
		Subject expected = getSub();
		assertTrue(actual.getName() == expected.getName());
		assertEquals(actual.getGrades(),expected.getGrades(), "Karakterene er ikke like");
	}
	
	

}
