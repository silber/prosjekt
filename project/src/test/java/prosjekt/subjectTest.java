package prosjekt;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import project.Student;
import project.Subject;

class subjectTest {
	private Student andrea;
	private Subject java;
	
	@BeforeEach
	public void setup() {
		andrea = new Student();
		java = new Subject();
		java.setName("java");
	}
	
	@Test
	public void testCalculateAverageGrade() {
		Subject sub = new Subject();
		java.addGrade(1);
		java.addGrade(2);
		java.addGrade(5);
		
		//Test at gjennomsnittskarakteren i Java er 'C'
		assertTrue(java.calculateAverageGrade() == 'C', "calculateAverageGrade returnerer feil gjennomsnittlig karakter");
		
		assertThrows(IllegalArgumentException.class, () -> {
			sub.calculateAverageGrade();
		}, "kastet ikke exception");
	
	}
	
	@Test
	public void testAddGrade() {
		assertThrows(IllegalArgumentException.class, () -> {
			java.addGrade(8);
		}, "kastet ikke exception");
		
		java.addGrade(2);
		assertTrue(java.getGrades().contains(2));
	}
	
	@Test 
	public void testConvertToChar() {
		assertThrows(IllegalArgumentException.class, () -> {
			java.addGrade(8);
		}, "kastet ikke exception");
		assertTrue(Subject.convertToChar(2) == 'D');
	}
	
	@Test 
	public void testConvertToInt() {
		assertTrue(Subject.convertToInt('C') == 3);
	}
	
}