package prosjekt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import project.Student;
import project.Subject;
import project.SubjectList;

public class StudentTest {
	
	private Student andrea;
	
	@BeforeEach
	public void setup() {
		andrea = new Student();
	}
	
	private Student getStudent() {
		Student andrea = new Student();
		andrea.setName("Andrea Huus");
		andrea.addSubject("Java", 'B');
		return andrea;
	}
	
	@Test
	public void testAddSubject() {
		List<Integer> subGrades = new ArrayList<Integer>();;
		
		//test at student ikke f�r legge til ugyldig karakter
		assertThrows(IllegalArgumentException.class, () -> {
			andrea.addSubject("Java", 'R');
		});
		
		//test at et fag ikke kan opprettes med special characters
		assertThrows(IllegalArgumentException.class, () -> {
			andrea.addSubject("jfidw0432//hfj*", 'B');
		});
		
		//test at alle karakterer A-F er lovlig
		for (char grades : Subject.getGradeMap().values()) {
			Assertions.assertDoesNotThrow(() -> andrea.addSubject("java", grades), "Fikk ikke legge til alle karakterer A-F");
		}
		
		//test at fag blir korrekt lagt til i studentens karakterliste
		andrea.addSubject("ProgSek",'B');
		assertTrue(andrea.getGrades().containsKey("ProgSek"), "Faget ble ikke korrekt lagt til i studenten sine karakterer.");
	
		//test at highest grade returnerer riktig verdi
		andrea.addSubject("Java", 'A');
		assertTrue(andrea.getGrades().get("Java") == 5, "Highest grade returnerer feil verdi");
		andrea.addSubject("Java", 'C');
		assertTrue(andrea.getGrades().get("Java") == 5, "Highest grade returnerer feil verdi");
	
		//test at karakteren 'A' legges til under java i subList
		assertTrue(SubjectList.getAllSubjects().containsKey("Java"), "Fag har ikke blitt lagt til i SubjectList");
		assertTrue(SubjectList.getAllSubjects().get("Java").getGrades().contains(5), "Java inneholder ikke karakteren 'A'");
	}
	
	@Test
	public void testAverageGrade() {
		andrea.addSubject("Java",'B');
		andrea.addSubject("komsys",'A');
		andrea.addSubject("ProgSek",'C');
		assertEquals(andrea.calculateAverageGrade(), 'B', "Average grade ble regnet ut feil");
	}
	
	@Test
	public void testGradestoString() {
		Student silje = new Student();
		assertThrows(IllegalArgumentException.class, () -> {
			silje.gradestoString();
		}, "kastet ikke exception");
	}
	
	@Test
	public void testWorstGrade() {
		//test at metoden returnerer IllegalArgument om ingen fag er lagt til
		Student silje = new Student();
		assertThrows(IllegalArgumentException.class, () -> {
			silje.worstGrade();
		}, "kastet ikke exception");
		
		andrea.addSubject("Progsek", 'A');
		andrea.addSubject("Objekt", 'C');
		
		//test at den verste karakteren er 'C'
		assertTrue(andrea.worstGrade() == 'C', "Verste karakter er feil");
		andrea.addSubject("Objekt", 'B');
		
		//Test at karakeren i "Objekt" har oppdatert seg og at verste karakter n� er 'B'
		assertTrue(andrea.worstGrade() == 'B', "Karakteren i Objekt har ikke oppdatert seg");
	}
	
	@Test
	public void testBestGrade() {
		//test at metoden returnerer IllegalArgument om ingen fag er lagt til
		Student silje = new Student();
		assertThrows(IllegalArgumentException.class, () -> {
			silje.bestGrade();
		}, "kastet ikke exception");
		
		andrea.addSubject("Progsek", 'C');
		andrea.addSubject("Objekt", 'C');
		
		//test at den verste karakteren er 'C'
		assertTrue(andrea.bestGrade() == 'C', "Beste karakter er feil");
		andrea.addSubject("Objekt", 'B');
		
		//Test at karakeren i "Objekt" har oppdatert seg og at verste karakter n� er 'B'
		assertTrue(andrea.bestGrade() == 'B', "Karakteren i Objekt har ikke oppdatert seg");
	}
}
