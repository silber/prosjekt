package project;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public interface IFile {
	Subject readGradelist(String name, InputStream input);
	
	Subject readGradelist(String name) throws IOException;
	
	void writeGradelist(char grade, OutputStream output);
	
	void writeGradelist(String subname, char grade) throws IOException;
		
	
	
}
