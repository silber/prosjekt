package project;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class GradeController {
	
	private Student student;
	private IFile fileSupport = new GradeFiles();
	private Subject subject;
	
	@FXML private TextField name, email, password, subname,grade, studname, statistics ;
	@FXML private TextArea output, suboutput, gradesoutput, statisticsOutput; 
	
	public void setGrades(Subject subject) {
		this.subject = subject;	
	}
	
	public void onAddSubject() {
		if(this.student == null) {
			suboutput.setText("Student name must be set before entering subjects");
		}
		try {
			String subname = this.subname.getText();
			char grade =  this.grade.getText().charAt(0);
			this.student.addSubject(subname, grade);
			System.out.println(this.student.gradestoString());
			fileSupport.writeGradelist(subname, grade);
			suboutput.setText("Saved to file");	
		}
		catch (IllegalArgumentException i) {
			suboutput.setText("Grade must be" +"\n" + " a character between A-F" + "\n" + "and the subject name can" + "\n" + "only contain letters and numbers");
		}
		catch (IOException e) {
		suboutput.setText("Unable to save to file");
		}	
	}
	
	public void onAddStudent() {
		String studname = this.studname.getText();
		this.student = new Student();
		this.student.setName(studname);
	}
	
	public void onAllGrades() {
		try {
			gradesoutput.setText(this.student.gradestoString());
		}
		catch(NullPointerException e) {
			
			gradesoutput.setText("Student name must be set");
		}
		catch(IllegalArgumentException i) {
			gradesoutput.setText("No subjects are added");
		}
 
	}
	
	public void onAverage() {
		try {
			String avg = "" + this.student.calculateAverageGrade(); 
			gradesoutput.setText(avg);
		
		}catch(NullPointerException e) {
		
		gradesoutput.setText("Student name and subject must be set");
		}
		catch(IllegalArgumentException i) {
			gradesoutput.setText("Student has no subjects");
		}	
	}

	public void onBest() {
		try {
			String best = "" + this.student.bestGrade();
			gradesoutput.setText(best);
		}
		catch(NullPointerException e) {
		
		gradesoutput.setText("Student name and subject must be set");
		}
		catch(IllegalArgumentException i) {
			gradesoutput.setText("Student has no subjects");
		}
		
	}
	
	public void onWorst() {
		try {
			String worst = "" + this.student.worstGrade();
			gradesoutput.setText(worst);
		}
		catch(NullPointerException e) {
			
			gradesoutput.setText("Student name and subject must be set");
		}
		catch(IllegalArgumentException i) {
			gradesoutput.setText("Student has no subjects");
		}
	}
	
	public void onStatistics() {
		try {
			String statistics = this.statistics.getText();
			String gradeString = "Karakterer i " + statistics + ": " + "\n ";
			setGrades(fileSupport.readGradelist(statistics));
			char avg = this.subject.calculateAverageGrade();
			
			for(int grade : subject.getGrades()) {
				gradeString += Subject.convertToChar(grade) + ", ";
			}
			
			gradeString += "\n" + "Gjennomsnittskarakteren er: " + avg;
			statisticsOutput.setText(gradeString);
		}
		catch(NullPointerException i) {
			statisticsOutput.setText("Subject does not exist");
		}
		catch(IOException e) {
			statisticsOutput.setText("Unable to read from file");
	
		}catch(IllegalArgumentException il) {
			statisticsOutput.setText("IllegalArgument");
		}
	}

}
