package project;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Scanner;


public class GradeFiles implements IFile{
	
	public final static String GRADELIST_EXTENSION = "Grades";
	
	private Path getUserGradelistFolderPath() {
		return Path.of(System.getProperty("user.home"), "git", "prosjekt", "project");	
	}
		
	
	private boolean ensureUserGradelistFolder() {
        try {
            Files.createDirectories(getUserGradelistFolderPath());
            return true;
        } catch (IOException ioe) {
            return false;
        }catch(InvalidPathException e) {
        	return false;
        }catch(IllegalArgumentException i) {
        	return false;
        }
    }
	private Path getGradelistPath(String name) {
        return getUserGradelistFolderPath().resolve(name + "." + GRADELIST_EXTENSION);
    }
	
	public Subject readGradelist(String name, InputStream input) {
		Subject subject = null;
        char grade;
        try (var scanner = new Scanner(input)) {
        	subject = new Subject();
            subject.setName(name);
            while (scanner.hasNextLine()) {
                var line = scanner.nextLine().stripTrailing();
                System.out.println(line);
                grade = line.charAt(0);
                if (line.isEmpty()) {
                    continue;
                }
                subject.addGrade(Subject.convertToInt(grade));
               
            }
            scanner.close();
        return subject;
        }
    }

    public Subject readGradelist(String name) throws IOException {
        var gradelistPath = getGradelistPath(name);
        try (var input = new FileInputStream(gradelistPath.toFile())) {
            return readGradelist(name, input);
        }
    }
	
	
    public void writeGradelist(char grade, OutputStream output) {
        try (var writer = new PrintWriter(output)) {
            writer.println(grade);
            System.out.println("klarte � skrive noe");
            writer.close();
        }
    }

    public void writeGradelist(String subname, char grade) throws IOException {
        var gradelistPath = getGradelistPath(subname);
        ensureUserGradelistFolder();
        try (var output = new FileOutputStream(gradelistPath.toFile(), true)) {
            writeGradelist(grade, output);
        
        }catch(FileNotFoundException e) {
        	System.out.println("Ugyldig fagnavn");
        	throw new IOException();
        	
        }
    }
	
	public static void main(String[] args) {
		GradeFiles fil = new GradeFiles();
		try {
			fil.writeGradelist("java", 'B');
			fil.writeGradelist("java", 'C');
			fil.writeGradelist("java", 'D');
			fil.writeGradelist("ProgSek", 'D');
			Subject subject = fil.readGradelist("java");
			System.out.println(subject.getGrades());
			
		} catch (IOException e) {
			System.out.println("klarte ikke skrive til fil");
			e.printStackTrace();
		}
		
		
	}
		    
	 
	
	
}
