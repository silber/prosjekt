package project;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Subject {
	private final static Map<Integer, Character> gradeMap = Map.of(0,'F',1,'E',2,'D',3,'C',4,'B',5,'A');
	private Student stud = new Student();
	private List<Integer> grades = new ArrayList<>();
	//private static SubjectList subList = new SubjectList();
	private String name;
	

	
	public Subject(String name, Integer grade) {
		this.name = name;
		grades.add(grade);
		//sub.computeIfAbsent(name, k -> new ArrayList<Integer>()).add(grade);
		
	}

	public Subject() {
			}

	public char calculateAverageGrade() {
		double averageGrade;
		int values = 0;
		if (grades.isEmpty()) {
			System.out.println("Du har ikke lagt til noen fag enda");
			throw new IllegalArgumentException("Ingen fag er lagt til");
		}
		else {
			for (int grade : grades) {
				values += grade;
			}
		}
		System.out.println("karakterer er til sammen " + values + "!");
		averageGrade = (float) values / grades.size();
		double h = averageGrade - Math.floor(averageGrade);
		char avg = h<0.5 ? Subject.convertToChar((int)Math.floor(averageGrade)) : Subject.convertToChar((int) Math.ceil(averageGrade));  
		System.out.println("Gjennomsnittskarakteren for " + this.name + " er: " + avg);
		return avg;

	}
	
	public void addGrade(int grade) {
		if(!gradeMap.containsKey(grade)) {
			throw new IllegalArgumentException("Grade must be a number between 0-5");
		}
		else {
			grades.add(grade);
		}
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Integer> getGrades(){
		return this.grades;
	}
	
	
	public String getName() {
		return this.name;
	}
	public static char convertToChar(int grade) {
		if(!gradeMap.containsKey(grade)) {
			throw new IllegalArgumentException("The grade must be a number between 0-5");
			
		}else {
			return gradeMap.get(grade);
		}
		
	}
	
	public static int convertToInt(char grade) {
		int converted = 0;
		for (int i = 0; i < gradeMap.size(); i++) {
			if (gradeMap.get(i) == grade) {
				converted = i;
				}
			}
		return converted;
	}
	
	public static Map<Integer, Character> getGradeMap() {
		return gradeMap;
	}
	
	
		
	
	public static void main(String[] args) {
		Subject Java = new Subject("Java", 2);
		Student andrea = new Student();
		andrea.addSubject("Java", 'A');
		andrea.addSubject("ITGK", 'A');
		andrea.addSubject("Java", 'B');
		
		
		
		
	}
	
	
	
}
