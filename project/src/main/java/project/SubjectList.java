package project;

import java.util.HashMap;
import java.util.Map;

public class SubjectList {
	
	//private static  Map<String, ArrayList<Integer>> allSubjects = new HashMap<String, ArrayList<Integer>>();

	private static Map<String, Subject> allSubjects = new HashMap<String, Subject>();

	
	public void addSubject(String name, int grade) {
		if(!Subject.getGradeMap().containsKey(grade)) {
			throw new IllegalArgumentException("Grade must be a number between 0-5");
		}
		if(allSubjects.containsKey(name)) {
			Subject sub = allSubjects.get(name);
			sub.addGrade(grade);
		}
		else{
			Subject sub1 = new Subject(name, grade);
			allSubjects.put(name, sub1);	
		}	
	}
	
	
	public static Map<String, Subject> getAllSubjects(){
		return allSubjects;
	}
	
	
}
