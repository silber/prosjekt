package project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

//import src.main.java.todolist.model.Iterable;
//import src.main.java.todolist.model.TodoEntry;

public class User implements Iterable<ArrayList>{
	
	private int studnr;
	private String email;
	private int phonenr;
	private Subject subject;
	
	
	//private List<Map.Entry<String,Character>> gradesList= new ArrayList<>();
	private static Map<String,Character> map = new HashMap<String,Character>();
	
	HashMap<Character, Integer> gradesMap;

	private Map<Character, Integer> gradeMap = Map.of('A', 5, 'B', 4, 'C', 3, 'D', 2, 'E', 1, 'F', 0);

	


	
	public int getStudnr() {
		return studnr;
	}
	/*private static void createMap() {
	   Map<Character,Integer> grades = new HashMap<Character, Integer>();
	    grades.put('F', 0);
	    grades.put('E', 1);
	    grades.put('D', 2);
	    grades.put('C', 2);

*/


	
	public void setStudnr(int studnr) {
		this.studnr = studnr;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPhonenr() {
		return phonenr;
	}
	public void setPhonenr(int phonenr) {
		this.phonenr = phonenr;
	}
	
	public void AddSubject(String sub, char grade) throws Exception {
		
		for (String i : map.keySet()) {
			if (map.containsKey(sub)){
				throw new Exception("Subject already added");
				
			}
			
			map.put(sub, grade);
			
		}
		
	}
	
	
	
	
	@Override
	public Iterator<ArrayList> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public static void main(String[] args) {
		
		map.put("TDT4322", 'C');
		map.put("TDT4342", 'B');
		map.put("TDT4300", 'C');


		map.putAll(map);
		System.out.println(map);
	}
	
	

}
