package project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Student {
	
	private String name;
	private Map<String, Integer> grades = new HashMap<String, Integer>();
	private static SubjectList subList = new SubjectList();

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addSubject(String subName, char grade) {
		int highestGrade;
		Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(subName);
		boolean b = m.find();
		if (b) {
			throw new IllegalArgumentException("The subject name can only contain letters and numbers");
		}
		if (!Subject.getGradeMap().containsValue(grade)) {
			throw new IllegalArgumentException(grade + " er ikke en gyldig karakter. Vennligst legg  til karakter mellom A og F.");
		}
		if (grades.containsKey(subName)) {
			System.out.println("Dette faget er allerede tatt. Karakteren vil legges til om den er bedre enn den eksisterende karakteren.");
			highestGrade = Math.max(Subject.convertToInt(grade), grades.get(subName));
			System.out.println("Den h�yeste karakteren er " + highestGrade);
			grades.put(subName, highestGrade);
		}
		else {
			grades.put(subName, Subject.convertToInt(grade));
		}
		
		subList.addSubject(subName, Subject.convertToInt(grade));
	}
	
	public Map<String, Integer> getGrades(){
		return grades;
	}
		
	public char calculateAverageGrade() {
		double averageGrade;
		int values = 0;
		
		if (grades.isEmpty()) {
			System.out.println("Du har ikke lagt til noen fag enda");
			throw new IllegalArgumentException("Ingen fag er lagt til");
		}
		else {
			for (int grade : grades.values()) {
				values += grade;
			}
		}
		System.out.println("karakterer er til sammen " + values + "!");
		averageGrade = (float) values / grades.size();
		double h = averageGrade - Math.floor(averageGrade);
		char avg = h<0.5 ? Subject.convertToChar((int)Math.floor(averageGrade)) : Subject.convertToChar((int) Math.ceil(averageGrade));  
		System.out.println("Gjennomsnittskarakteren er: " + avg);
		System.out.println(grades.size());
		return avg;

	}
	/*
	public char calculateMedian() {
		int middle;
		char medianGrade = 0;
		List<Integer> sortedGrades = new ArrayList<>();
		if (grades.isEmpty()) {
			throw new NullPointerException("Du har ikke lagt til noen fag enda");
		}
		else {
			for (int grade : grades.values()) {
				sortedGrades.add(grade);
			}
			Collections.sort(sortedGrades);
			middle = (sortedGrades.get(sortedGrades.size()/2) + sortedGrades.get(sortedGrades.size()/2 - 1))/2;
			medianGrade = Subject.convertToChar(sortedGrades.get(middle));
			System.out.println("Mediankarakteren din er: " + medianGrade);
			
			
		}
		return medianGrade;
	}
	*/
	public String gradestoString() {
		String st="";
		if(grades.isEmpty() || grades.equals(null)) {
			throw new IllegalArgumentException("No subjects are added");}
		for(String grade : this.grades.keySet()) {
			st += grade + " : " + Subject.convertToChar(this.grades.get(grade)) + "\n";
			}
		return st;
	}
	
		
	public char worstGrade() {
		char bokstav = 0;
		int min = Integer.MAX_VALUE; 
		List<String> minKeys = new ArrayList<> ();
		if(grades.isEmpty()) {
			throw new IllegalArgumentException("No subjects are added");
		}
		for(Map.Entry<String, Integer> entry : grades.entrySet()) {
		    if(entry.getValue() < min) {
		        min = entry.getValue();
		        minKeys.clear();
		    }
		    if(entry.getValue() == min) {
		        minKeys.add(entry.getKey());
		        int karakter = entry.getValue();
		        bokstav = Subject.convertToChar(karakter);
		    }
		}
		
		System.out.println("Din værste karakter er " + minKeys + " og karakteren er " + bokstav);	
		return bokstav;
	}
	
	public char bestGrade() {
		int max = Integer.MIN_VALUE;
		char bokstav = 0;
		List<String> maxKeys = new ArrayList<> ();
		if(grades.isEmpty()) {
			throw new IllegalArgumentException("No subjects are added");
		}
		for(Map.Entry<String, Integer> entry : grades.entrySet()) {
		    if(entry.getValue() > max) {
		    	max = entry.getValue();
		    	maxKeys.clear();
			    }
		    if(entry.getValue() == max) {
		    	maxKeys.add(entry.getKey());
		    	int karakter = entry.getValue();
		    	bokstav = Subject.convertToChar(karakter);  
			    }
			}
		
		System.out.println("Din beste karakter er " + maxKeys + " og karakteren er " + bokstav);
		return bokstav;
				
		}
	
	
	
	public static void main(String[] args) {
		Student silje = new Student();
		Student andrea = new Student();
        //silje.addSubject("Java", 'B');
		silje.addSubject("Matte", 'A');
		
		silje.addSubject("MMI", 'D');
		silje.addSubject("Java", 'A');
		silje.addSubject("Java", 'A');
		andrea.addSubject("Java", 'B');

		silje.addSubject("Makro", 'B');
		silje.addSubject("Tjenestedesign", 'C');
		//silje.addSubject("ITGK", 'D');

		silje.addSubject("EiT", 'F');
		//silje.addSubject("EiT67*.i", 'F');
		silje.addSubject("Finans", 'F');
		//silje.addSubject("Matte", 'A');
		silje.calculateAverageGrade();
		silje.worstGrade();
		silje.bestGrade();
		Map<String,Integer> grades = silje.getGrades();
		System.out.println("Silje sine karakterer er: " + grades);
		Subject fag = subList.getAllSubjects().get("Java");
		System.out.println(fag.getGrades());
		fag.calculateAverageGrade();	
		String hei = silje.toString();
		System.out.println(hei);
		System.out.println(grades);
		
		
	}
}


